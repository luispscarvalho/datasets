This is a place for all my datasets. Each branch of this repo is dedicate to a particular step of my research and/or paper:

**JBCS2020_EvolutionaryUniformity**

Contains the dataset of the paper for the Journal of the Brazilian Compute Society: "Relationships between Design Problem Agglomerations and Concerns Having Types and Domains of Software as Transverse Dimensions"

**UFBA2020_Thesis**

This branch contains dataset and scripts used in my thesis: "Software Concerns from Third-Party Components' Metadata"

**JSERD2023_ConcernsFromThirdPartyComponents**

The dataset of the paper: "Software Concerns from Third-Party Components' Metadata"


